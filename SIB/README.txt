--Subscription info backend--

Author: Taavi Luik

bitbucket repo: https://bitbucket.org/taavi_luik/sib

--Running--

Run with CLI: java -jar SIB.jar

--Overview--

The server will run and listen to incoming queries. It expects JSON files with 
'type' key signalling the type of query (currently only supports 'report') and 
'id' key for the id (currently phone number) of the client being queried.

The application has 4 main parts: 
1) SIB - the entry point of the program that starts the server.

2) SimpleServer - listens for incoming socket connections on localhost on port 
2222. Once a connection is made, a ClientHandler object is created and a thread 
is allocated for the client. The server goes back to waiting for new connections.

3) ClientHandler - provides the runnable that listens for incoming messages on 
a given socket. Once a messages is recieved, it's passed to the Responder class.
The ClientHandler then goes back to listening for incoming messages.

4) SimpleResponder - checks the validity of the query. If it's valid, asks the data
layer for required data, asks the handler for a response runnable that is then
passed to the thread allocated to outgoing messages.


--Testing--
Start the server and run the CommunicationTest.
CommunicationTest creates a client socket that connects to the server and verifies 
the connection exists and data is sent and recieved.

--Developing--
Database, Server, Responder and Handler are interfaces. A new implementation
can be provided, it just has to be bound in the MainModule class.
