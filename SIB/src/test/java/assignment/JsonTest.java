package assignment;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

import org.junit.Test;

import com.google.gson.Gson;

import assignment.domain.Report;
import assignment.domain.Service;
import assignment.domain.Subscription;

public class JsonTest {

	@Test
	public void TestJsonParsing() {
		Gson gson = new Gson();

		Service service = new Service();
		service.setName("TestName");
		service.setProvider("TestProvider");

		Report r = new Report();
		r.setNextService(service);
		r.setNextBillingDate(LocalDate.of(2017, 8, 1));

		Subscription sub1 = new Subscription(), sub2, sub3;
		sub1.setCost(new BigDecimal(3.99));
		sub1.setDate(LocalDate.of(2017, 5, 1));
		sub1.setPeriod(Period.ofMonths(1));
		sub1.setService(service);
		sub2 = new Subscription(sub1);
		sub2.setDate(sub1.getDate().plusMonths(1));
		sub3 = new Subscription(sub1);
		sub3.setDate(sub1.getDate().plusMonths(2));
		r.addSubscription(sub1);
		r.addSubscription(sub2);
		r.addSubscription(sub3);
		System.out.println(gson.toJson(r));
		
	}
}
