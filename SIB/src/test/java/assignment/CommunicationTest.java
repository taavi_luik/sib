package assignment;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.Gson;

import assignment.server.Query;
import assignment.server.SimpleServer;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CommunicationTest {
	private static ExecutorService pool;
	private BufferedWriter out;

	@BeforeClass
	public static void setup() {
		pool = Executors.newSingleThreadExecutor();
	}

	@AfterClass
	public static void dispose() {
		Utils.dispose(pool);
	}

	@Test
	public void SendQuery() {
		String response = null;
		try {
			Socket socket = new Socket("localhost", SimpleServer.port);
			assertTrue(socket.isConnected());

			pool.submit(() -> generateDelayedQueries(1500, socket));
			response = waitForReply(socket, 3500);
			log.debug("Recieved response: " + response);
		} catch (IOException e) {
			log.error("", e);
		}
		assertTrue(response != null);
	}
	private void generateDelayedQueries(int delayInMillis,
			Socket socket) {
		Query query = new Query();
		query.putParameter("type", "report");
		query.putParameter("id", "123456789");
		Gson gson = new Gson();

		try {
			log.debug("About to send msg: " + gson.toJson(query));
			try {
				if (out == null)
					out = new BufferedWriter(new OutputStreamWriter(
							socket.getOutputStream(), StandardCharsets.UTF_8));
				Thread.sleep(delayInMillis);
				log.debug(
						"Sending msg!");
				out.write(gson.toJson(query));
				out.newLine();
				out.flush();
				log.debug(
						"Sent msg!");
			} catch (InterruptedException e) {
				log.error("", e);
			}

		} catch (IOException e) {
			log.error("Shit.", e);
		}
	}

	private String waitForReply(Socket s, int timeoutInMillis) {
		log.debug("Starting to wait for reply for " + timeoutInMillis + " ms");
		String line;
		try (BufferedReader in = new BufferedReader(
				new InputStreamReader(s.getInputStream(),
						"UTF-8"))) {
			s.setSoTimeout(timeoutInMillis);
			while (true) {
				log.debug("listening");
				while ((line = in.readLine()) != null) {
					log.debug("Got the message: " + line);
					return line;
				}
			}

		} catch (IOException e) {
			log.error("Shit.", e);
		}
		return null;
	}
}
