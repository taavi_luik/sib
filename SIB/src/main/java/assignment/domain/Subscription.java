package assignment.domain;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.Period;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
/**
 * A single subscription to a Service with a cost, begin date and period. Cost
 * (BigDecimal) accompanied by a String representation for brevity.
 * 
 * @author Taavi Luik
 *
 */
@Accessors(chain = true)
public class Subscription {

	@Getter private transient BigDecimal cost;
	@Getter @Setter private Service service;
	@Getter @Setter private LocalDate date;
	@Getter @Setter private Period period;
	@Getter @Setter private String costValue;

	public Subscription() {

	}

	/**
	 * A copy constructor. Note: BigDecimal, LocalDate and Period are immutable,
	 * Service is not.
	 * 
	 * @param parent
	 */
	public Subscription(Subscription parent) {
		this.cost = parent.cost;
		this.costValue = formatDecimal(parent.cost);
		this.service = parent.service;
		this.date = parent.date;
		this.period = parent.period;
	}
	private String formatDecimal(BigDecimal cost) {
		return NumberFormat.getCurrencyInstance().format(cost);
	}

	public Subscription setCost(BigDecimal dec) {
		this.cost = dec;
		this.costValue = formatDecimal(dec);
		return this;
	}

}
