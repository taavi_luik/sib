package assignment.domain;

import java.time.LocalDate;
import java.util.HashSet;

import lombok.Getter;
import lombok.Setter;

/**
 * A class that holds together the subscription metadata queried by the client.
 * 
 * @author Taavi Luik
 *
 */
public class Report {

	@Getter @Setter private LocalDate nextBillingDate;
	@Getter @Setter private Service nextService;
	@Getter @Setter private HashSet<Subscription> subscriptions;

	public Report() {
		subscriptions = new HashSet<>();
	}

	public void addSubscription(Subscription... subs) {
		for (Subscription sub : subs) {
			subscriptions.add(sub);
		}
	}
}
