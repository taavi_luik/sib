package assignment;
import com.google.inject.Guice;
import com.google.inject.Injector;

import assignment.server.Server;
/**
 * Main class of the server. Sets up Guice, creates and starts a Server object.
 * 
 * @author Taavi Luik
 *
 */
public class SIB {
	private static Server server;
	public static Injector injector;

	public static void main(String[] args) {
		injector = Guice.createInjector(new MainModule());
		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				server.stop();
			}
		});
		server = injector.getInstance(Server.class);
		server.start();
	}

}
