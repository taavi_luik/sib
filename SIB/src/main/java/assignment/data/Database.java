package assignment.data;

import assignment.domain.Report;
import assignment.server.Query;
/**
 * A very basic interface for a data layer that returns data when queried.
 * 
 * @author Taavi Luik
 *
 */
public interface Database {

	public Report getReport(Query q);
}
