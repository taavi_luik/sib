package assignment.data;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;

import com.google.inject.Singleton;

import assignment.domain.Report;
import assignment.domain.Service;
import assignment.domain.Subscription;
import assignment.server.Query;
import lombok.extern.slf4j.Slf4j;

/**
 * Data layer implementation that serves static assets.
 * 
 * @author Taavi Luik
 *
 */
@Slf4j
@Singleton
public class StaticAssets implements Database {

	@Override
	public Report getReport(Query q) {
		Report report = null;
		if (q.getParameter("id").equals("123456789")) {
			report = new Report();

			Service s = new Service();
			s.setName("Premium Account");
			s.setProvider("GO Server");

			report.setNextBillingDate(LocalDate.of(2017, 8, 1));
			Subscription sub1 = new Subscription(), sub2, sub3, sub4, sub5;
			sub1.setCost(new BigDecimal(3.99))
					.setDate(LocalDate.of(2017, 1, 1))
					.setPeriod(Period.ofMonths(1))
					.setService(s);

			sub2 = new Subscription(sub1).setDate(sub1.getDate().plusMonths(1));
			sub3 = new Subscription(sub1).setDate(sub1.getDate().plusMonths(2));
			sub4 = new Subscription(sub1).setDate(sub1.getDate().plusMonths(3));
			sub5 = new Subscription(sub1).setDate(sub1.getDate().plusMonths(4));

			report.addSubscription(sub1, sub2, sub3, sub4, sub5);
			log.debug("Returning created report for id 123456789");
		}

		return report;
	}

}
