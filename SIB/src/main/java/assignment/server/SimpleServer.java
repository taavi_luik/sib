package assignment.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import com.google.inject.Singleton;

import assignment.SIB;
import assignment.Utils;
import lombok.extern.slf4j.Slf4j;
/**
 * An implementation of the central class of the backend, the Server.
 * 
 * Needs the start() method to be called, which starts listening for incoming
 * connections. If a connection is found, a handler is created that handles the
 * incoming data of that particular client.
 * 
 * Also features a method for queueing outgoing data on a single thread.
 * 
 * @author Taavi Luik
 *
 */
@Slf4j
@Singleton
public class SimpleServer implements Server {

	public static int port = 2222;
	private ServerSocket serverSocket;

	/**
	 * Thread pool of 2 threads for registering incoming connections and
	 * outgoing messages.
	 */
	private ExecutorService serverPool;
	/**
	 * Cached thread pool for individual client connections.
	 */
	private ExecutorService clientPool;

	public SimpleServer() {
		serverPool = Executors.newFixedThreadPool(2);
		clientPool = Executors.newCachedThreadPool();
	}
	@Override
	public void start() {
		System.out.println("Starting server.");
		try {
			serverSocket = new ServerSocket(port);
			serverPool.submit(listenForConnections());
		} catch (IOException e) {
			log.error("Failed!", e);
		}

	}

	@Override
	public void stop() {
		System.out.println("Stopping server.");
		Utils.dispose(serverPool);
		Utils.dispose(clientPool);
		try {
			serverSocket.close();
		} catch (IOException e) {
			log.error("Failed to close ServerSocket", e);
		}
	}

	private Runnable listenForConnections() {
		return () -> {
			System.out.println("Listening for incoming connections.");
			while (true) {
				try {
					Socket socket = serverSocket.accept();
					if (socket != null) {
						log.debug("Found incoming connection!");
						ClientHandler client = SIB.injector
								.getInstance(ClientHandler.class);
						client.setSocket(socket);
						clientPool.submit(client.listenForInput());
					} else {
					}
				} catch (Exception e) {
					log.error("", e);
				}
			}
		};
	}
	@Override
	public void queueResponse(Runnable r) {
		serverPool.submit(r);
	}

}
