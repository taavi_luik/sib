package assignment.server;

/**
 * A Java representation of an incoming JSON object from the client. 
 * Has a validity check method and a map of parameter name to value.
 * @author Taavi Luik
 */
import java.util.HashMap;

import lombok.Getter;

public class Query implements Message {
	@Getter private HashMap<String, String> parameters;

	public Query() {
		parameters = new HashMap<>();
	}

	public void putParameter(String key, String value) {
		parameters.put(key, value);
	}
	public String getParameter(String key) {
		return parameters.get(key);
	}

	public boolean isValid(String queryType) {
		if (queryType.equals("report")) {
			return parameters.get("id") != null;
		}
		return false;
	}
}
