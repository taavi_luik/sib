package assignment.server;

public interface Server {

	public void start();
	public void stop();
	public void queueResponse(Runnable r);

}
