package assignment.server;

/**
 * An interface for the object that takes care of reacting to incoming queries.
 * 
 * @author Taavi Luik
 *
 */
public interface Responder {

	public void respond(String content, ClientHandler client);

}
