package assignment.server;
/**
 * Interface for the handler that is supposed to provide runnables for a given
 * client: a looping method that listens for incoming messages and handles them
 * and a method that transmits a response.
 * 
 * @author Taavi Luik
 *
 */
public interface Handler {

	public Runnable listenForInput();
	public Runnable getRunnableFor(String response);
}
