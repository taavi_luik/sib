package assignment.server;
/**
 * The interface for the base object that is transmitted between the client and
 * server.
 * 
 * @author Taavi Luik
 *
 */
public interface Message {

	public void putParameter(String key, String value);
	public String getParameter(String key);
	public boolean isValid(String type);

}
