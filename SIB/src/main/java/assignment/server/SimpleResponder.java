package assignment.server;

import com.google.gson.Gson;
import com.google.inject.Inject;
import com.google.inject.Singleton;

import assignment.data.Database;
import assignment.domain.Report;

/**
 * A basic implementation of the Responder interface. Checks the validity of the
 * query, retrieves the answer from the data layer, asks the handler for a
 * runnable that is passed to the thread allocated for outgoing messages in the
 * server object.
 * 
 * @author Taavi Luik
 *
 */
@Singleton
public class SimpleResponder implements Responder {

	private Database data;
	private Server server;
	private Gson gson;

	@Inject
	public void setServer(Server server) {
		this.server = server;
	}
	@Inject
	public void setDatabase(Database database) {
		this.data = database;
	}

	public SimpleResponder() {
		gson = new Gson();
	}

	public void respond(String content, ClientHandler client) {
		Query query = parseMessage(content);
		if (query.isValid(query.getParameter("type"))) {
			String answer = getAnswer(query);
			queueResponse(answer, client);
		}
	}

	private String getAnswer(Query query) {
		if (query.getParameter("type").equals("report")) {
			return gson.toJson(generateReportFor(query));
		}
		return null;
	}
	private void queueResponse(String response, ClientHandler client) {
		server.queueResponse(client.getRunnableFor(response));
	}

	private Query parseMessage(String content) {
		return gson.fromJson(content, Query.class);
	}

	private Report generateReportFor(Query q) {
		return data.getReport(q);
	}
}
