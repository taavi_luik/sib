package assignment.server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

import com.google.inject.Inject;

import lombok.extern.slf4j.Slf4j;
/**
 * Provides runnables that listen for the client for incoming messages. Passes
 * messages further and provides a response runnable.
 * 
 * @author Taavi Luik
 *
 */
@Slf4j
public class ClientHandler implements Handler {
	private Socket socket;
	private Responder responder;
	private BufferedWriter out;
	private BufferedReader br;

	@Inject
	public void setResponder(Responder responder) {
		this.responder = responder;
	}

	public void setSocket(Socket socket) {
		this.socket = socket;
	}

	@Override
	public Runnable listenForInput() {
		return () -> {
			String content;
			try {
				if (br == null)
					br = new BufferedReader(
							new InputStreamReader(socket.getInputStream()));
				while (true) {
					while (socket.isConnected()
							&& (content = br.readLine()) != null) {
						log.debug("Got query: " + content);
						responder.respond(content, this);
					}
				}
			} catch (Exception e) {
				log.error("Shit.", e);
			}
		};
	}

	@Override
	public Runnable getRunnableFor(String response) {
		return () -> {
			try {
				if (out == null)
					out = new BufferedWriter(new OutputStreamWriter(
							socket.getOutputStream(), StandardCharsets.UTF_8));
				out.write(response);
				out.newLine();
				out.flush();
			} catch (IOException e) {
				log.error("Something went wrong!", e);
			}
		};
	}
}
