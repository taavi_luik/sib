package assignment;

import com.google.inject.Binder;
import com.google.inject.Module;

import assignment.data.Database;
import assignment.data.StaticAssets;
import assignment.server.ClientHandler;
import assignment.server.Handler;
import assignment.server.Responder;
import assignment.server.Server;
import assignment.server.SimpleResponder;
import assignment.server.SimpleServer;
/**
 * Guice module that binds interfaces to concrete implementations.
 * 
 * @author Taavi Luik
 *
 */
public class MainModule implements Module {

	@Override
	public void configure(Binder binder) {
		binder.bind(Server.class).to(SimpleServer.class);
		binder.bind(Database.class).to(StaticAssets.class);
		binder.bind(Responder.class).to(SimpleResponder.class);
		binder.bind(Handler.class).to(ClientHandler.class);
	}

}
