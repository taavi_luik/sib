package assignment;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class Utils {

	public static void dispose(ExecutorService pool) {
		if (pool != null) {
			try {
				pool.awaitTermination(200, TimeUnit.MILLISECONDS);
			} catch (InterruptedException e) {
				log.error("Error at disposing executor.", e);
			} finally {
				if (!pool.isShutdown()) {
					pool.shutdownNow();
				}
			}
		}
	}
}
